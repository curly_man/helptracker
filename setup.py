from setuptools import setup, find_packages
import os

setup(
    name='helptracker',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        'jsonpickle==0.9.6',
        'python-dateutil==2.7.3',
    ],
    entry_points={
        'console_scripts':
            ['ht = command_interface.console:main']
    },
    author='Tony',

)
