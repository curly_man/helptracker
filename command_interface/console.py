"""
    Tris module is used to work with library.
"""

import argparse
import jsonpickle
import os.path, sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
from solution import Manager, Logger
from config import Config


# from logger import Logger
# from task import Task
# from user import User


def add_user(args, manager, parser):
    try:
        if args.login:
            manager.add_user(args.login)
        else:
            parser._subparsers._group_actions[0].choices['user']\
            ._subparsers.group_actions[0].choices['add']\
                .print_help()
    except Exception as e:
        print(e)


def delete_user(args, manager, parser):
    try:
        if not args.id:
            raise Exception('Yo don\'t write user id. Use -i + user id')
        manager.delete_user(args.id)
    except Exception as e:
        print(e)


def print_users(args, manager, parser):
    try:
        if args.all:
            users = manager.print_all_users()
            for _id in users:
                print(users[_id])
        elif args.current:
            user = manager.print_current_user()
            print(user.login + ' ' + str(user.id))
        elif args.tag:
            tags = manager.print_user_tag()
            for tag in tags:
                print('#' + tag)
        else:
            raise Exception(
                'Not found argument! \n -a (--all) to print all users \n -c (--cur) to print current user \n'
                ' -t (--tag) to print tags current user')
    except Exception as e:
        print(e)


def chose_user(args, manager, parser):
    try:
        if not args.id:
            raise Exception('Yo dont write user id. Use -i + user id')
        manager.chose_current_user(args.id)
    except Exception as e:
        print(e)


def add_task(args, manager, parser):
    try:
        if not args.subtask:
            manager.add_user_task(name=args.name, priority=args.priority, period=args.period, finish=args.finish,
                                  deadline=args.deadline, description=args.description)
        else:
            if not args.id:
                raise Exception('Yo dont write task id. Use -i + user id')
            manager.add_user_subtask(_id=args.id, name=args.name, priority=args.priority, period=args.period,
                                     finish=args.finish, deadline=args.deadline, description=args.description)
    except Exception as e:
        print(e)


def change_task(args, manager, parser):
    try:
        if not args.id:
            raise Exception('Yo dont change. Use -i + task id')
        manager.change_user_task(args.id, name=args.name, priority=args.priority, finish=args.finish,
                                 description=args.description, deadline=args.deadline, period=args.period)
    except Exception as e:
        print(e)


def delete_task(args, manager, parser):
    try:
        if not args.id:
            raise Exception('Yo dont delete task. Use -i + task id')
        manager.delete_user_task(args.id)
    except Exception as e:
        print(e)


def complete_task(args, manager, parser):
    try:
        if not args.id:
            raise Exception('Yo dont complete task. Use -i + task id')
        manager.complete_user_task(args.id)
    except Exception as e:
        print(e)


def task_tag(args, manager, parser):
    try:
        if not args.id:
            raise Exception('Yo dont input task id. Use -i + task id')
        if args.add is True:
            manager.add_user_task_tag(args.id, str(args.name))
        elif args.delete is True:
            manager.delete_user_task_tag(args.id, str(args.name))
    except Exception as e:
        print(e)


def print_tasks(args, manager, parser):
    try:
        if args.all:
            print_task(manager.print_user_tasks())
        elif args.tag:
            print_task(manager.print_user_tasks_by_tag(args.tag))
        elif args.archived:
            print_task(manager.print_user_archived_tasks())
        else:
            raise Exception(
                'Not found argument! \n -a (--all) to print all tasks \n -t (--tag) to print tasks by tag \n'
                ' -a (--ar) to print tags current user')
    except Exception as e:
        print(e)


def add_project(args, manager, parser):
    try:
        manager.add_project(args.name)
    except Exception as e:
        print(e)


def print_projects(args, manager, parser):
    try:
        projects = manager.print_projects()
        for _id in projects:
            print(projects[_id].name + ' ' + str(projects[_id].id))
    except Exception as e:
        print(e)


def chose_project(args, manager, parser):
    try:
        if not args.id:
            raise Exception('Yo dont chose project. Use -i + project id')
        manager.chose_current_project(args.id)
    except Exception as e:
        print(e)


def add_project_user(args, manager, parser):
    try:
        if args.login:
            manager.add_project_user(args.login)
        elif args.id:
            manager.add_project_available_user(args.id)
    except Exception as e:
        print(e)


def delete_project_user(args, manager, parser):
    try:
        if args.id:
            manager.delete_project_user(args.id)
    except Exception as e:
        print(e)


def project_print_users(args, manager):
    try:
        users = manager.get_project_users()
        for _id in users:
            print(users[_id])
    except Exception as e:
        print(e)


def add_project_task(args, manager, parser):
    try:
        if args.id:
            manager.add_project_available_task(args.id)
        elif args.subtask is False:
            manager.add_project_task(name=args.name, priority=args.priority, finish=args.finish,
                                     description=args.description)
    except Exception as e:
        print(e)


def delete_project_task(args, manager, parser):
    try:
        manager.delete_project_task(args.id)
    except Exception as e:
        print(e)


def project_complete_task(args, manager, parser):
    try:
        manager.project_complete_task(args.id)
    except Exception as e:
        print(e)


def project_change_task(args, manager, parser):
    try:
        manager.project_change_task(args.id, name=args.name, status=args.status, priority=args.priority,
                                    start=args.start,
                                    finish=args.finish, description=args.description)
    except Exception as e:
        print(e)


def print_project_tasks(args, manager, parser):
    try:
        if args.all:
            print_task(manager.print_project_tasks())
        elif args.archived:
            print_task(manager.project_print_archived_tasks())
    except Exception as e:
        print(e)


def print_func(task, tab='\t'):
    print('        -----------------------------------------------------------------')
    print(tab + '| ' + task.name + ' id:' + str(task.id))
    print(tab + '|. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .')
    print(tab + '| Description: ' + str(task.description))
    print(tab + '| Task Start: ' + str(task.start.strftime('%Y-%m-%d %H:%M')))
    print(tab + '| Task Deadline: ' + str(task.deadline))
    print(tab + '| Task Finish: ' + str(task.finish))
    print(tab + '| Status:' + str(task.status) + ' Priority:' + str(task.priority))
    print(tab + '| Task Period: ' + str(task.period))
    print(tab + '| Archived:' + str(task.archived))


def print_task1(tasks):
    print('  Task count:' + str(len(tasks)))
    for _id in tasks:
        if tasks[_id].subtask.count == 0 and tasks[_id].parent is False:
            if tasks[_id].archived is not True:
                print_func(task=tasks[_id])
        elif tasks[_id].parent is False:
            print_sub(task=tasks[_id])
    print('        -----------------------------------------------------------------')


def print_task(tasks):
    print('  Task count:' + str(len(tasks)))
    for _id in tasks:
        if tasks[_id].subtask.count == 0 and tasks[_id].parent is False:
            print_func(task=tasks[_id])
        elif tasks[_id].parent is False:
            print_sub(task=tasks[_id])
    print('        -----------------------------------------------------------------')


def print_sub(task, tab="\t"):
    print_func(task, tab=tab)
    for _id in task.subtask.tasks:
        if task.subtask.tasks[_id].subtask:
            print_sub(task=task.subtask.tasks[_id], tab=('\t' + '|' + tab))
        else:
            if task.archived is not True:
                print_func(task=task.subtask.tasks[_id], tab=(tab + '|'))


def default_func(manager, args, parser):
    try:
        if sys.argv[1] == 'console.py':
            parser._subparsers._group_actions[0].choices[sys.argv[1]].print_help()
        else:
            parser._subparsers._group_actions[0].choices[sys.argv[1]].print_help()
    except:
        parser.print_help()


def parse(manager):
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=default_func)
    subparsers = parser.add_subparsers()

    user = subparsers.add_parser('user', help='To work with users')
    user.set_defaults(func=default_func)
    user_parser = user.add_subparsers(dest='action', help='Methods for work with users')

    add_user_parser = user_parser.add_parser('add', help=('Add User.'))
    add_user_parser.add_argument('-l', '--login', help=('user login'))
    add_user_parser.set_defaults(func=add_user)

    delete_user_parser = user_parser.add_parser('del', help=("Delete User."))
    delete_user_parser.add_argument('-i', '--id', help=('user id'))
    delete_user_parser.set_defaults(func=delete_user)

    chose_user_parser = user_parser.add_parser('chose', help=('Chose Current User.'))
    chose_user_parser.add_argument('-i', '--id', help=('user id'))
    chose_user_parser.set_defaults(func=chose_user)

    print_user_parser = user_parser.add_parser('print', help=('Print All Users.'))
    print_user_parser.add_argument('-a', '--all', help=('Print All Users.'), action='store_true')
    print_user_parser.add_argument('-c', '--current', help=('Print Current User.'), action='store_true')
    print_user_parser.add_argument('-t', '--tag', help=('Print all user tags.'), action='store_true')
    print_user_parser.set_defaults(func=print_users)

    task = subparsers.add_parser('task', help='To work with tasks')
    task_parser = task.add_subparsers(dest='action', help='Methods for work with tasks')

    add_task_parser = task_parser.add_parser('add', help=('Add Task.'))
    add_task_parser.add_argument('-i', '--id', help=('task id'))
    add_task_parser.add_argument('-n', '--name', help=('name of task'), default='New Task')
    add_task_parser.add_argument('-s', '--status', help=('status of task [0, 1, 2, 3]'), default=0)
    add_task_parser.add_argument('-pr', '--priority', help=('priority of task [0, 1, 2]'), default=1)
    add_task_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    add_task_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    add_task_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    add_task_parser.add_argument('-sub', '--subtask', help=('to add this task how subtask to task with id'),
                                 action='store_true')
    add_task_parser.add_argument('-d', '--deadline', help='Deadline of task', default=None)
    add_task_parser.add_argument('-p', '--period', help='Period of task', default=None)
    add_task_parser.set_defaults(func=add_task)

    delete_task_parser = task_parser.add_parser('del', help=('Delete Task.'))
    delete_task_parser.add_argument('-i', '--id', help=('task id'))
    delete_task_parser.set_defaults(func=delete_task)

    complete_task_parser = task_parser.add_parser('complete', help=('Complete task.'))
    complete_task_parser.add_argument('-i', '--id', help=('id task that completed.'))
    complete_task_parser.set_defaults(func=complete_task)

    change_task_parser = task_parser.add_parser('change', help=('Change Task.'))
    change_task_parser.add_argument('-i', '--id', help=('task id'))
    change_task_parser.add_argument('-n', '--name', help=('name of task'))
    change_task_parser.add_argument('-s', '--status', help=('status of task [0, 1, 2, 3]'), default=0)
    change_task_parser.add_argument('-pr', '--priority', help=('priority of task [0, 1, 2]'), default=0)
    change_task_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    change_task_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    change_task_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    change_task_parser.add_argument('-d', '--deadline', help='Deadline of task', default=None)
    change_task_parser.add_argument('-p', '--period', help='Period of task', default=None)
    change_task_parser.set_defaults(func=change_task)

    task_tag_parser = task_parser.add_parser('tag', help=("Add Task Tag."))
    task_tag_parser.add_argument('-i', '--id', help=('task id'))
    task_tag_parser.add_argument('-n', '--name', help=('name of tag'))
    task_tag_parser.add_argument('-a', '--add', help=('add task tag'), action='store_true')
    task_tag_parser.add_argument('-d', '--delete', help=('delete task tag'), action='store_true')
    task_tag_parser.set_defaults(func=task_tag)

    print_task_parser = task_parser.add_parser('print', help=('Print Tasks.'))
    print_task_parser.add_argument('-a', '--all', help=('print all tasks'), action='store_true')
    print_task_parser.add_argument('-t', '--tag', help=('print tags tasks'))
    print_task_parser.add_argument('-ar', '--archived', help=('print archived tasks'), action='store_true')
    print_task_parser.set_defaults(func=print_tasks)

    project = subparsers.add_parser('project', help='To work with project')
    project_parser = project.add_subparsers(dest='action', help='Methods for work with projects')

    chose_project_parser = project_parser.add_parser('chose', help=('To chose current project'))
    chose_project_parser.add_argument('-i', '--id', help=('id right project'))
    chose_project_parser.set_defaults(func=chose_project)

    project_add_parser = project_parser.add_parser('add', help=('To add project'))
    project_add_parser.add_argument('-n', '--name', help=('name of project'))
    project_add_parser.set_defaults(func=add_project)

    project_print_parser = project_parser.add_parser('print', help=('Print all projects'))
    project_print_parser.set_defaults(func=print_projects)

    user = project_parser.add_parser('user', help='To work with users')
    user_parser = user.add_subparsers(dest='action', help='Methods for work with users', metavar='')

    add_user_parser = user_parser.add_parser('add', help=('Add User.'))
    add_user_parser.add_argument('-l', '--login', help=('user login'))
    add_user_parser.add_argument('-i', '--id', help=('user id'))
    add_user_parser.set_defaults(func=add_project_user)

    delete_user_parser = user_parser.add_parser('del', help=("Delete User."))
    delete_user_parser.add_argument('-i', '--id', help=('user id'))
    delete_user_parser.set_defaults(func=delete_project_user)

    print_user_parser = user_parser.add_parser('print', help=('Print All Users.'))
    print_user_parser.set_defaults(func=project_print_users)

    task = project_parser.add_parser('task', help='To work with tasks')
    task_parser = task.add_subparsers(dest='action', help='Methods for work with tasks', metavar='')

    add_task_parser = task_parser.add_parser('add', help=('Add Task.'))
    add_task_parser.add_argument('-i', '--id', help=('task id'))
    add_task_parser.add_argument('-n', '--name', help=('name of task'), default='New Task')
    add_task_parser.add_argument('-s', '--status', help=('status of task [0, 1, 2, 3]'), default=0)
    add_task_parser.add_argument('-pr', '--priority', help=('priority of task [0, 1, 2]'), default=1)
    add_task_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    add_task_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    add_task_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    add_task_parser.add_argument('-sub', '--subtask', help=('to add this task how subtask to task with id'),
                                 action='store_true')
    add_task_parser.add_argument('-p', '--period', help=('period of task [0, 1, 2]'), default=1)
    add_task_parser.add_argument('-d', '--deadline', help=('priority of task [0, 1, 2]'), default=1)
    add_task_parser.set_defaults(func=add_project_task)

    delete_task_parser = task_parser.add_parser('del', help=('Delete Task.'))
    delete_task_parser.add_argument('-i', '--id', help=('task id'))
    delete_task_parser.set_defaults(func=delete_project_task)

    complete_task_parser = task_parser.add_parser('complete', help=('Complete task.'))
    complete_task_parser.add_argument('-i', '--id', help=('id task that completed.'))
    complete_task_parser.set_defaults(func=project_complete_task)

    change_task_parser = task_parser.add_parser('change', help=('Change Task.'))
    change_task_parser.add_argument('-i', '--id', help=('task id'))
    change_task_parser.add_argument('-n', '--name', help=('name of task'))
    change_task_parser.add_argument('-s', '--status', help=('status of task [0, 1, 2, 3]'), default=0)
    change_task_parser.add_argument('-pr', '--priority', help=('priority of task [0, 1, 2]'), default=0)
    change_task_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    change_task_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    change_task_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    change_task_parser.set_defaults(func=project_change_task)

    print_task_parser = task_parser.add_parser('print', help=('Print Tasks.'))
    print_task_parser.add_argument('-a', '--all', help=('print all tasks'), action='store_true')
    print_task_parser.add_argument('-t', '--tag', help=('print tags tasks'))
    print_task_parser.add_argument('-ar', '--archived', help=('print archived tasks'), action='store_true')
    print_task_parser.set_defaults(func=print_project_tasks)

    return parser


def main():
    config = Config(None)
    folder = str(config.get_setting('Files', 'data_folder'))
    mngr = str(config.get_setting('Files', 'manager'))
    log = str(config.get_setting('Files', 'logger'))
    manager = Manager(folder=folder, mngr=mngr)
    manager.check_finish()
    my_logger = Logger(log)
    parser = parse(manager)

    args = parser.parse_args()
    try:
        args.func(args=args, manager=manager, parser=parser)
    except Exception as e:
        print(e)
    # print(args)

    # write_manager(manager, folder, mngr)

    # Logger.get_all_logger_output_messages(Logger)


if __name__ == '__main__':
    main()
