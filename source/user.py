from alltasks import AllTasks
from additional_files.status import STATUS
from additional_files.priority import PRIORITIES


class User:
    def __init__(self, login):
        self.login = login
        self.tasks = AllTasks()
        self.tags = []
        self.id = hash(self)

    def add_task(self, task):
        self.tasks.add(task)

    def add_subtask(self, _id, task):
        self.tasks.add_sub(_id, task)

    def delete_task(self, _id):
        self.tasks.delete(_id)

    def complete_task(self, _id):
        self.tasks.complete(_id)

    def add_task_tag(self, tag, _id):
        if tag not in self.tags:
            self.tags.append(tag)
        self.tasks.add_tag(tag, _id)

    def delete_task_tag(self, tag, _id):
        self.tasks.delete_tag(tag, _id)

    def print_tags(self):
        for tag in self.tags:
            print('#' + tag)

    def print_tasks(self):
        self.tasks.print_tasks()

    def print_tasks_by_tag(self, tag):
        self.tasks.print_by_tag(tag)

    def print_archived_tasks(self):
        self.tasks.print_archive_tasks()

    def change_task(self, _id, name=None, status=None, priority=None, start=None, finish=None, description=None):
        if name:
            self.tasks[_id].name = name
        if status:
            self.tasks[_id].status = STATUS[status]
        if priority:
            self.tasks[_id].priority = PRIORITIES[(int(priority))]
        if start:
            self.tasks[_id].start = start
        if finish:
            self.tasks[_id].finish = finish
        if description:
            self.tasks[_id].description = description
