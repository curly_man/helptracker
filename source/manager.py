from task import Task
from alltasks import AllTasks
from user import User
from logger import Logger
from additional_files.status import STATUS
from additional_files.priority import PRIORITIES
import os
import jsonpickle


class Manager:
    def __init__(self):
        self.current_user = None
        self.current_project = None
        self.users = {}
        self.projects = {}

    """
        Methods to work with users
    """

    def add_user(self, login):
        user = User(login)
        self.users[user.id] = user
        Logger.get_logger().info('Add user {0} with id:{1}'.format(user.login, user.id))

    def delete_user(self, _id):
        if _id in self.users:
            del self.users[_id]
            Logger.get_logger().info('Delete user:{0}'.format(self.users.pop(_id).login))
        else:
            Logger.get_logger().error('User with id:{0} not found!'.format(_id))
            raise Exception('Wrong id!')

    def chose_current_user(self, _id):
        if _id in self.users:
            user = self.users[_id]
            self.current_user = user
            Logger.get_logger().info('Chose current user:{0}'.format(self.current_user.login))
        else:
            Logger.get_logger().error('User with id:{0} not found!'.format(_id))
            raise Exception('Wrong id!')

    def print_current_user(self):
        print(self.current_user.login + ' ' + str(self.current_user.id))
        Logger.get_logger().info('Print current user')

    def print_all_users(self):
        for _id in self.users:
            print(self.users[_id].login + ' ' + str(self.users[_id].id))
        Logger.get_logger().info('Print all users')

    def print_user_tag(self):
        self.current_user.print_tags()

    def write_users(self):
        pickle = jsonpickle.dumps(self.users)
        with open(os.path.join(os.path.dirname(os.getcwd()), 'data', 'users.json'), 'w+') as file:
            file.write(pickle)

    def read_users(self):
        try:
            if os.path.exists(os.path.join(os.path.dirname(os.getcwd()), 'data', 'users.json')):
                with open(os.path.join(os.path.dirname(os.getcwd()), 'data', 'users.json'), 'r+') as file:
                    self.users = jsonpickle.loads(file.read())
        except:
            print('USERS.JSON don\'t have information')

    def write_current_user(self):
        temp = jsonpickle.dumps(self.current_user)
        with open(os.path.join(os.path.dirname(os.getcwd()), 'data', 'current_user.json'), 'w+') as file:
            file.write(temp)

    def read_current_user(self):
        try:
            if os.path.exists(os.path.join(os.path.dirname(os.getcwd()), 'data', 'current_user.json')):
                with open(os.path.join(os.path.dirname(os.getcwd()), 'data', 'current_user.json'), 'r+') as file:
                    self.current_user = jsonpickle.loads(file.read())
        except:
            print('CURRENT_USER.JSON don\'t have information')

    """
        Methods to work with tasks
    """

    def add_user_task(self, name=None, status=0, priority=1, start=None, finish=None, description=None):
        task = Task(name, int(status), int(priority), start, finish, description)
        self.current_user.add_task(task)
        Logger.get_logger().info('Task (id:{0}) added to user {1}'.format(task.id, self.current_user.login))

    def add_user_subtask(self, _id, name=None, status=0, priority=1, start=None, finish=None, description=None):
        task = Task(name, int(status), int(priority), start, finish, description)
        self.current_user.add_subtask(_id, task)
        Logger.get_logger().info('SubTask (id:{0}) added to task (id:{1})  (user {2})'.format(task.id, _id, self.current_user.login))

    def delete_user_task(self, _id):
        self.current_user.delete_task(_id)
        Logger.get_logger().info('Task (id:{0}) delete'.format(_id))

    def change_user_task(self, _id, name=None, status=None, priority=None, start=None, finish=None, description=None):
        self.current_user.change_task(_id, name, status, priority, start, finish, description)
        Logger.get_logger().info()

    def complete_user_task(self, _id):
        self.current_user.complete_task(_id)

    def add_user_task_tag(self, _id, tag):
        self.current_user.add_task_tag(tag, _id)

    def delete_user_task_tag(self, _id, tag):
        self.current_user.delete_task_tag(tag, _id)

    def print_user_tasks(self):
        self.current_user.print_tasks()

    def print_user_tasks_by_tag(self, tag):
        self.current_user.print_tasks_by_tag(tag)

    def print_user_archived_tasks(self):
        self.current_user.print_archived_tasks()

    """
        Methods to work with projects
    """

    def add_project_user(self, _id):
        self.current_project.add_user(self.users[_id])

    def delete_project_user(self, _id):
        self.current_project.delete_user(_id)

    def project_print_users(self):
        self.current_project.print_all_users()

    def add_project_task(self, name=None, status=0, priority=1, start=None, finish=None, description=None):
        task = Task(name, int(status), int(priority), start, finish, description)
        self.current_project.add_task(task)
        Logger.get_logger().info('Task (id:{0}) added to user {1}'.format(task.id, self.current_user.login))

    def add_user_subtask(self, _id, name=None, status=0, priority=1, start=None, finish=None, description=None):
        task = Task(name, int(status), int(priority), start, finish, description)
        self.current_project.add_subtask(_id, task)
        Logger.get_logger().info('SubTask (id:{0}) added to task (id:{1})  (user {2})'.format(task.id, _id, self.current_user.login))

    def delete_project_task(self, _id):
        self.current_project.delete_task(_id)
        Logger.get_logger().info('Task (id:{0}) delete'.format(_id))

    def project_change_task(self, _id, name=None, status=None, priority=None, start=None, finish=None, description=None):
        self.current_project.change_task(_id, name, status, priority, start, finish, description)
        Logger.get_logger().info()

    def project_complete_task(self, _id):
        self.current_project.complete_task(_id)

    def print_project_tasks(self):
        self.current_project.print_tasks()

    def print_project_tasks_by_tag(self, tag):
        self.current_project.print_tasks_by_tag(tag)

    def project_print_archived_tasks(self):
        self.current_project.print_archived_tasks()

    def write_projects(self):
        pickle = jsonpickle.dumps(self.projects)
        with open(os.path.join(os.path.dirname(os.getcwd()), 'data', 'projects.json'), 'w+') as file:
            file.write(pickle)

    def read_projects(self):
        try:
            if os.path.exists(os.path.join(os.path.dirname(os.getcwd()), 'data', 'projects.json')):
                with open(os.path.join(os.path.dirname(os.getcwd()), 'data', 'projects.json'), 'r+') as file:
                    self.users = jsonpickle.loads(file.read())
        except:
            print('projects.JSON don\'t have information')

    def write_current_project(self):
        temp = jsonpickle.dumps(self.current_user)
        with open(os.path.join(os.path.dirname(os.getcwd()), 'data', 'current_project.json'), 'w+') as file:
            file.write(temp)

    def read_current_project(self):
        try:
            if os.path.exists(os.path.join(os.path.dirname(os.getcwd()), 'data', 'current_project.json')):
                with open(os.path.join(os.path.dirname(os.getcwd()), 'data', 'current_project.json'), 'r+') as file:
                    self.current_user = jsonpickle.loads(file.read())
        except:
            print('CURRENT_PROJECT.JSON don\'t have information')