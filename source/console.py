import argparse
from manager import Manager
from logger import Logger


# from task import Task
# from user import User


def add_user(args, manager):
    manager.add_user(args.login)
    # manager.print_all_users()


def delete_user(args,manager):
    manager.delete_user(args.id)
    # manager.print_all_users()


def print_users(args, manager):
    if args.all is True:
        manager.print_all_users()
    elif args.current is True:
        manager.print_current_user()
    elif args.tag is True:
        manager.print_user_tag()


def chose_user(args, manager):
    manager.chose_current_user(args.id)
    manager.print_all_users()


def add_task(args, manager):
    try:
        if args.subtask is False:
            manager.add_user_task(name=args.name, status=args.status, priority=args.priority,
                                  start=args.start, finish=args.finish, description=args.description)
        else:
            manager.add_user_subtask(_id=args.id, name=args.name, status=args.status, priority=args.priority,
                                     start=args.start, finish=args.finish, description=args.description)
    except Exception as e:
        print(e)
        # manager.current_user.print_all_task()


def change_task(args, manager):
    try:
        manager.current_user.change_user_task(args.id, name=args.name, priority=args.priority, start=args.start,
                                              finish=args.finish, description=args.description)
    except Exception as e:
        print(e)
        manager.current_user.print_all_task()


def delete_task(args, manager):
    manager.delete_user_task(args.id)


def complete_task(args, manager):
    manager.complete_user_task(args.id)


def task_tag(args, manager):
    if args.add is True:
        manager.add_user_task_tag(args.id, str(args.name))
    elif args.delete is True:
        manager.delete_user_task_tag(args.id, str(args.name))


def print_tasks(args, manager):
    if args.all:
        manager.print_user_tasks()
    elif args.tag:
        manager.print_user_tasks_by_tag(args.tag)
    elif args.archived:
        manager.print_user_archived_tasks()


def parse(manager):
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    user = subparsers.add_parser('user', help='To work with users')
    user_parser = user.add_subparsers(dest='action', help='Methods for work with users', metavar='')

    add_user_parser = user_parser.add_parser('add', help=('Add User.'))
    add_user_parser.add_argument('-l', '--login', help=('user login'))
    add_user_parser.set_defaults(func=add_user)

    delete_user_parser = user_parser.add_parser('del', help=("Delete User."))
    delete_user_parser.add_argument('-i', '--id', help=('user id'))
    delete_user_parser.set_defaults(func=delete_user)

    chose_user_parser = user_parser.add_parser('chose', help=('Chose Current User.'))
    chose_user_parser.add_argument('-i', '--id', help=('user id'))
    chose_user_parser.set_defaults(func=chose_user)

    print_user_parser = user_parser.add_parser('print', help=('Print All Users.'))
    print_user_parser.add_argument('-a', '--all', help=('Print All Users.'), action='store_true')
    print_user_parser.add_argument('-c', '--current', help=('Print Current User.'), action='store_true')
    print_user_parser.add_argument('-t', '--tag', help=('Print all user tags.'), action='store_true')
    print_user_parser.set_defaults(func=print_users)

    task = subparsers.add_parser('task', help='To work with tasks')
    task_parser = task.add_subparsers(dest='action', help='Methods for work with tasks', metavar='')

    add_task_parser = task_parser.add_parser('add', help=('Add Task.'))
    add_task_parser.add_argument('-i', '--id', help=('task id'))
    add_task_parser.add_argument('-n', '--name', help=('name of task'), default='New Task')
    add_task_parser.add_argument('-s', '--status', help=('status of task [0, 1, 2, 3]'), default=0)
    add_task_parser.add_argument('-pr', '--priority', help=('priority of task [0, 1, 2]'), default=1)
    add_task_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    add_task_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    add_task_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    add_task_parser.add_argument('-sub', '--subtask', help=('to add this task how subtask to task with id'), action='store_true')
    add_task_parser.set_defaults(func=add_task)

    delete_task_parser = task_parser.add_parser('del', help=('Delete Task.'))
    delete_task_parser.add_argument('-i', '--id', help=('task id'))
    delete_task_parser.set_defaults(func=delete_task)

    complete_task_parser= task_parser.add_parser('complete', help=('Complete task.'))
    complete_task_parser.add_argument('-i', '--id', help=('id task that completed.'))
    complete_task_parser.set_defaults(func=complete_task)

    change_task_parser = task_parser.add_parser('change', help=('Change Task.'))
    change_task_parser.add_argument('-i', '--id', help=('task id'))
    change_task_parser.add_argument('-n', '--name', help=('name of task'))
    change_task_parser.add_argument('-s', '--status', help=('status of task [0, 1, 2, 3]'), default=0)
    change_task_parser.add_argument('-pr', '--priority', help=('priority of task [0, 1, 2]'), default=0)
    change_task_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    change_task_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    change_task_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    change_task_parser.set_defaults(func=change_task)

    task_tag_parser = task_parser.add_parser('tag', help=("Add Task Tag."))
    task_tag_parser.add_argument('-i', '--id', help=('task id'))
    task_tag_parser.add_argument('-n', '--name', help=('name of tag'))
    task_tag_parser.add_argument('-a', '--add', help=('add task tag'), action='store_true')
    task_tag_parser.add_argument('-d', '--delete', help=('delete task tag'), action='store_true')
    task_tag_parser.set_defaults(func=task_tag)

    print_task_parser = task_parser.add_parser('print', help=('Print Tasks.'))
    print_task_parser.add_argument('-a', '--all', help=('print all tasks'), action='store_true')
    print_task_parser.add_argument('-t', '--tag', help=('print tags tasks'))
    print_task_parser.add_argument('-ar', '--archived', help=('print archived tasks'), action='store_true')
    print_task_parser.set_defaults(func=print_tasks)

    """
    add_user_parser = subparsers.add_parser('useradd', help=('Add User.'))
    # add_user_parser.add_argument('-l', '--login', help=('user login'))
    add_user_parser.set_defaults(func=add_user)

    delete_user_parser = subparsers.add_parser('userdel', help=("Delete User."))
    delete_user_parser.add_argument('-i', '--id', help=('user id'))
    delete_user_parser.set_defaults(func=delete_user)

    chose_user_parser = subparsers.add_parser('userchose', help=('Chose Current User.'))
    chose_user_parser.add_argument('-i', '--id', help=('user id'))
    chose_user_parser.set_defaults(func=chose_user)

    print_users_parser = subparsers.add_parser('userprint', help=('Print All Users.'))
    print_users_parser.add_argument('-a', '--all', help=('Print All Users.'), action='store_true')
    print_users_parser.add_argument('-c', '--current', help=('Print Current User.'), action='store_true')
    print_users_parser.set_defaults(func=print_users)

    add_task_parser = subparsers.add_parser('taskadd', help=('Add Task.'))
    add_task_parser.add_argument('-n', '--name', help=('name of task'), default='New Task')
    add_task_parser.add_argument('-pg', '--progress', help=('progress of task'), default=0)
    add_task_parser.add_argument('-pr', '--priority', help=('priority of task'), default=0)
    add_task_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    add_task_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    add_task_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    add_task_parser.add_argument('-s', '--subtask', help=('to add'))
    add_task_parser.set_defaults(func=add_task)

    subtask_parser = subparsers.add_parser('subtask', help=('To Work With Subtasks'))
    subtask_parser.add_argument('-a', '--add', help=('add subtask'), action='store_true')
    subtask_parser.add_argument('-d', '--delete', help='delete subtask', action='store_true')
    subtask_parser.add_argument('-i', '--id', help=('id to task'))
    subtask_parser.add_argument('-n', '--name', help=('name of task'), default='New Task')
    subtask_parser.add_argument('-pg', '--progress', help=('progress of task'), default=0)
    subtask_parser.add_argument('-pr', '--priority', help=('priority of task'), default=0)
    subtask_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    subtask_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    subtask_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    subtask_parser.add_argument('-s', '--subtask', help=('to add'))
    subtask_parser.set_defaults(func=subtask_func)

    change_task_parser = subparsers.add_parser('taskchange', help=('Change Task.'))
    change_task_parser.add_argument('-i', '--id', help=('task id'))
    change_task_parser.add_argument('-n', '--name', help=('name of task'))
    change_task_parser.add_argument('-pg', '--progress', help=('progress of task'), default=0)
    change_task_parser.add_argument('-pr', '--priority', help=('priority of task'), default=0)
    change_task_parser.add_argument('-st', '--start', help=('start of task'), default=None)
    change_task_parser.add_argument('-end', '--finish', help=('end of task'), default=None)
    change_task_parser.add_argument('-ds', '--description', help=('description of task'), default=None)
    change_task_parser.set_defaults(func=change_task)

    task_tag_parser = subparsers.add_parser('tasktag', help=("Add Task Tag."))
    task_tag_parser.add_argument('-i', '--id', help=('task id'))
    task_tag_parser.add_argument('-n', '--name', help=('name of tag'))
    task_tag_parser.add_argument('-a', '--add', help=('add task tag'), action='store_true')
    task_tag_parser.add_argument('-d', '--delete', help=('delete task tag'), action='store_true')
    task_tag_parser.set_defaults(func=task_tag)

    delete_task_parser = subparsers.add_parser('taskdel', help=('Delete Task.'))
    delete_task_parser.add_argument('-i', '--id', help=('task id'))
    delete_task_parser.set_defaults(func=delete_task)

    print_task_parser = subparsers.add_parser('taskprint', help=('Print Tasks.'))
    print_task_parser.add_argument('-a', '--all', help=('print all user tasks'))
    print_task_parser.add_argument('-t', '--tag', help=('print tags tasks'))
    print_task_parser.set_defaults(func=print_tasks)
    """

    args = parser.parse_args()
    args.func(args, manager)


def main():
    manager = Manager()
    manager.read_current_user()
    manager.read_users()
    my_logger = Logger()
    my_logger.on_logger()
    # UsersSet.add_user(UsersSet.current_user)
    parse(manager)
    manager.write_current_user()
    manager.write_users()
    # Logger.get_all_logger_output_messages(Logger)


if __name__ == '__main__':
    main()
