class AllUsers:
    def __init__(self):
        self.users = {}

    def add_user(self, user):
        self.users[user.id] = user

    def delete_user(self, _id):
        if _id in self.users:
            self.users.pop(_id)
        else:
            raise Exception('Wrong id!')

    def chose_current_user(self, _id):
        return self.users[_id]

    def print_all_users(self):
        for _id in self.users:
            print(self.users[_id].login + ' ' + str(self.users[_id].id))

