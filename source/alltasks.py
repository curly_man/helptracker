class AllTasks:
    def __init__(self):
        self.tasks = {}
        self.count = 0

    def add(self, task):
        self.tasks[task.id] = task
        self.count = self.count + 1

    def delete(self, _id):
        if _id in self.tasks:
            del self.tasks[_id]
            self.count = self.count - 1
        else:
            raise Exception('Wrong id!')

    def add_sub(self, _id, task):
        if _id in self.tasks:
            task.parent = True
            self.tasks[_id].subtask.add(task)
            self.add(task)
        else:
            raise Exception('Wrong id!')

    def complete(self, _id):
        if _id in self.tasks:
            self.tasks[_id].archived = True
            self.tasks[_id].status = STATUS[1]
            if self.tasks[_id].subtask is not None:
                self.complete_subtask(self.tasks[_id].subtask.tasks)
        else:
            raise Exception('Wrong id!')

    def complete_subtask(self, tasks):
        for _id in tasks:
            self.complete(_id)

    def add_tag(self, tag, _id):
        self.tasks[_id].tags.append(tag)

    def delete_tag(self, tag, _id):
        self.tasks[_id].tags.remove(tag)

    def print_func(self, task, tab='\t'):
        print('        -----------------------------------------------------------------')
        print(tab + '| ' + task.name + ' id:' + str(task.id))
        print(tab + '| Status:' + str(task.status) + ' Priority:' + str(task.priority))
        print(tab + '| Archived:' + str(task.archived))

    def print_tasks(self):
        print('  Task count:' + str(self.count))
        for _id in self.tasks:
            if self.tasks[_id].subtask.count == 0 and self.tasks[_id].parent is False:
                if self.tasks[_id].archived is not True:
                    self.print_func(task=self.tasks[_id])
            elif self.tasks[_id].parent is False:
                self.print_sub(task=self.tasks[_id])
        print('        -----------------------------------------------------------------')

    def print_sub(self, task, tab="\t"):
        self.print_func(task, tab=tab)
        for _id in task.subtask.tasks:
            if task.subtask.tasks[_id].subtask:
                self.print_sub(task=task.subtask.tasks[_id], tab=('\t' + '|' + tab))
            else:
                if task.archived is not True:
                    self.print_func(task=task.subtask.tasks[_id], tab=(tab + '|'))

    def print_all_task(self):
        for _id in self.tasks:
            self.print_func(self.tasks[_id])

    def print_by_tag(self, tag):
        print('#' + tag + ':')
        for _id in self.tasks:
            if tag in self.tasks[_id].tags:
                self.print_func(self.tasks[_id])

    def print_archive_tasks(self):
        for _id in self.tasks:
            if self.tasks[_id].archived is True:
                self.print_func(self.tasks[_id])


