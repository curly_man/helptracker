import datetime
from additional_files.status import STATUS
from additional_files.priority import PRIORITIES
from alltasks import AllTasks


class Task:
    def __init__(self, name=None, status=0, priority=1, start=None, finish=None,
                 description=None):
        self.name = name
        self.status = STATUS[status]
        self.priority = PRIORITIES[priority]
        if start is None:
            self.start = datetime.datetime.now()
        else:
            self.start = start
        self.finish = finish
        self.description = description
        self.parent = False
        self.id = hash(self)
        self.tags = []
        self.subtask = AllTasks()
        self.archived = False
