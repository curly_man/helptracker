import logging
import os


path = os.path.dirname(__file__)+'output_logger_path.txt'


class Logger:
    def __init__(self, output_path=path, handler=None):
        self.output_path = output_path
        self.add_handler(handler)

    def add_handler(self, handler):
        logger = logging.getLogger('logging')
        logger.setLevel(logging.DEBUG)
        if not handler:
            new_handler = logging.FileHandler(self.output_path)
            new_handler.setLevel(logging.DEBUG)
            new_handler.setFormatter(logging.Formatter('[%(asctime)s] - %(levelname)s - %(filename)s - '
                                                       '[LINE:%(lineno)d] - %(message)s', datefmt='%Y-%m-%d %H:%M:%S'))
            logger.addHandler(new_handler)
        else:
            if type(handler) == logging.FileHandler:
                logger.addHandler(handler)

    @classmethod
    def get_logger(cls):
        return logging.getLogger('logging')

    def on_logger(self):
        logger = self.get_logger()
        logger.disabled = False

    def off_logger(self):
        logger = self.get_logger()
        logger.disabled = True

    def get_all_logger_output_messages(self):
        """Return records all actions in program
        The read files is specified in the configuration file. (default=logger_output.txt)

        :return: list of messages
        """
        messages = []

        with open(path) as file:
            for line in file:
                messages.append(line[:-1])

        return messages
