STATUS = tuple(enumerate([
    'In progress',
    'Completed',
    'Failed',
    'Periodic'
]))
