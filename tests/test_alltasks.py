import unittest
from dateutil.parser import *
import os.path, sys

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
from solution import AllTasks, Task, STATUS, Manager


class TestAllTask(unittest.TestCase):

    def setUp(self):
        self.tasks = AllTasks()

    def test_add_task(self):
        task = Task(name='Task1')
        self.tasks.add(task)
        self.assertIn(task.id, self.tasks.tasks)

    def test_delete_task(self):
        task = Task(name='Task1')
        self.tasks.add(task)
        self.tasks.delete(task.id)
        self.assertNotIn(task.id, self.tasks.tasks)

    def test_change_task(self):
        task = Task('Task1')
        self.tasks.add(task)
        self.tasks.change_task(task.id, name='Task2')
        self.assertEqual(self.tasks.get_task(task.id).name, 'Task2')

    def test_add_subtask(self):
        task1 = Task(name='Task1')
        self.tasks.add(task1)
        task2 = Task(name='Task2')
        self.tasks.add_sub(task1.id, task2)
        self.assertIn(task2.id, task1.subtask.tasks)

    def test_complete_task(self):
        task = Task('Task1')
        self.tasks.add(task)
        self.tasks.complete(task.id)
        self.assertEqual(task.status, STATUS[1])

    def test_check_finish(self):
        finish = parse('2018-07-18 12:00:00')
        task1 = Task(name='task', finish=finish)
        self.tasks.add(task1)
        self.tasks.check_task_finish(task1)
        self.assertEqual(task1.status, STATUS[2])

    def test_start_task(self):
        parsed_finish = parse('2018-11-18 12:00:00', dayfirst=True)
        parsed_deadline = parse('2018-10-18 12:00:00', dayfirst=True)
        parsed_new_deadline = parse('2018-10-19 12:00:00', dayfirst=True)
        parsed_period = Manager.get_time_period('d')
        task = Task(name='task', finish=parsed_finish, deadline=parsed_deadline, period=parsed_period)
        self.tasks.add(task)
        self.tasks.complete(task.id)
        self.assertEqual(self.tasks.get_task(task.id).deadline, parsed_new_deadline)


if __name__ == "__main__":
    unittest.main()
