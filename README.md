# HelpTracker
## HelpTracker is an application that will help you organize your tasks and plans.


## This application contains:

* package 'command_interface': this is console application using library tracker

* package 'tests': tests for library

* library 'solution': source code

## Requires
* jsonpickle 0.9.6
* python-dateutil 2.7.3


## Installation
To install this application open terminal, go to the package folder and enter: "python setup.py install"
After that console application was installed on your computer.

To run applicetion in console use ht

To use library in program import motule source


# LIBRARY DESCRIPTION
Tracker can:

### Work with tasks

1. Add task

2. Add subtask

3. Changed task

4. Complete tasks

5. Delete task

6. Work with periodic tasks

7. Add tags

8. Delete tags

### Work with projects

1. Add project

2. Add user to project(new/available)

3. Delete user from project

4. Add task(new/available) to project