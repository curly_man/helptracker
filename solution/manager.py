"""
It is intended for management of all requests of the program

Each functions:
1. Gets manager from the storage
2. Verifies the correctness of the data
3. Raises an exception on errors
4. Created/removed users
5. Created/removed tasks
6. Created/removed projects
7. Return manager in the storage

Public Methods:

'add_user': add new user in storage
'delete_user': delet user from storage
'chose_current_user': chose current users from all users
'print_all_users': print all users
'print_current_users': print current user
'print_user_tag': print current user tags
'add_user_task': add new task in 'user/project'
'delete_user_task': removed task in 'user/project'
'add_user_subtask': add new subtask to task
'change_user_subtask': change task
'complete_task': complete task
'add_task_tag': add tag to task
'delete_task_tag': delete tag from task
'print_user_tasks': get tasks
'print_tasks_by_tag': get tasks with tag
'print_archived_tasks': get archive tasks
'add_project': add project
'print_projects': print projects
'chose_current_project': chose current_project
'add_project_user': add uset to project
'add_project_available_user': add available user to project
'add_project_task': add task to project
'add_project_available_task': add available task to project
'delete_project_user': delete user from project
'delete_project_task': delete task from project
'get_users': get project users
'get_tasks': get project tasks

users - all program users
current_user - id current user
projects - all program projects
current_project - id current project
"""

from solution.task import Task
from solution.user import User
from solution.logger import Logger
from solution.priority import PRIORITIES
from datetime import datetime, timezone
from solution.project import Project
from dateutil.relativedelta import *
from dateutil.parser import *
from solution.storage import Storage


class Manager:
    def __init__(self, folder, mngr):
        self.storage = Storage(folder, mngr)
        obj = self.storage.read_manager()
        if obj:
            self.users = obj.users
            self.projects = obj.projects
            self.current_user = obj.current_user
            self.current_project = obj.current_project
        else:
            self.users = {}
            self.projects = {}
            self.current_user = None
            self.current_project = None


    def check_finish(self):
        if self.current_user:
            return self.users[self.current_user].check_finish()

    def add_user(self, login):
        if login:
            user = User(login)
            self.users[user.id] = user
            self.current_user = str(user.id)
            self.storage.write_manager(self)
            Logger.get_logger().info('Add user {0} with id:{1}'.format(user.login, user.id))
        else:
            raise Exception('login is none')

    def delete_user(self, _id):
        if _id in self.users:
            del self.users[_id]
            self.storage.write_manager(self)
            Logger.get_logger().info('Delete user:{0}'.format(_id))
        else:
            Logger.get_logger().error('User with id:{0} not found!'.format(_id))
            raise Exception('Wrong id! user not delete')

    def chose_current_user(self, _id):
        if _id in self.users:
            self.current_user = _id
            self.storage.write_manager(self)
            Logger.get_logger().info('Chose current user:{0}'.format(self.users[self.current_user].login))
        else:
            Logger.get_logger().error('User with id:{0} not found!'.format(_id))
            raise Exception('Wrong id!')

    def print_current_user(self):
        if self.current_user:
            Logger.get_logger().info('Print current user')
            user = self.users[self.current_user]
            return user
        else:
            Logger.get_logger().error('no current user')
            raise Exception('Current user is None')

    def print_all_users(self):
        if not self.users:
            Logger.get_logger().error('no users')
            raise Exception('No users')
        Logger.get_logger().info('Print all users')
        return self.users

    def print_user_tag(self):
        if self.current_user:
            tags = self.users[self.current_user].print_tags()
            if tags:
                Logger.get_logger().info('Print user tags')
                return tags
            else:
                Logger.get_logger().error('Print user tags(user dont have tags)')
        else:
            Logger.get_logger().error('no current user')
            raise Exception('Current user is None')



    def add_task(self, name=None, priority=1, finish=None, description=None,
                 deadline=None, period=None):
        """
            Function to make a task
            :param name: task name
            :param priority: task priority
            :param finish: task finish
            :param description: task description
            :param deadline: task deadline
            :param period: task period
            :return: task
        """
        if not deadline and period:
            Logger.get_logger().error('Cannot add task because task is periodic and no deadline.')
            raise Exception('if task periodic, you must input deadline')

        if finish:
            try:
                parsed_finish = parse(finish, dayfirst=True)
            except:
                raise Exception('Wrong format of date')
        else:
            parsed_finish = finish

        if deadline:
            try:
                parsed_deadline = parse(deadline, dayfirst=True)
            except:
                raise Exception('Wrong format of date')
        else:
            parsed_deadline = deadline

        if int(priority) > 2 or int(priority) < 0:
            Logger.get_logger().error('Priority {0} doesn\'t exist'.format(priority))
            raise Exception('Wrong priority!')

        parsed_period = self.get_time_period(period)
        task = Task(name=name, priority=(priority), finish=parsed_finish, description=description,
                    deadline=parsed_deadline, period=parsed_period)
        return task

    def add_user_task(self, name=None, priority=1, finish=None, description=None,
                      deadline=None, period=None):
        """
            Function to add a task to user
            :param name: task name
            :param priority: task priority
            :param finish: task finish
            :param description: task description
            :param deadline: task deadline
            :param period: task period
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot add task because user not authorized.')
            raise Exception('You haven\'t authorized')
        task = self.add_task(name=name, priority=priority, finish=finish, description=description,
                             deadline=deadline, period=period)
        self.users[self.current_user].add_task(task)
        self.storage.write_manager(self)
        Logger.get_logger().info('Task (id:{0}) added to user {1}'.format(task.id, self.users[self.current_user].login))

    def add_user_subtask(self, _id, name=None, priority=1, finish=None, description=None,
                         period=None, deadline=None):
        """
            Function to add a a subtask
            :param name: task name
            :param priority: task priority
            :param finish: task finish
            :param description: task description
            :param deadline: task deadline
            :param period: task period
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot add subtask because user not authorized.')
            raise Exception('You haven\'t authorized')
        task = self.add_task(name=name, priority=priority, finish=finish, description=description,
                             deadline=deadline, period=period)
        self.users[self.current_user].add_subtask(_id, task)
        self.storage.write_manager(self)
        Logger.get_logger().info('SubTask (id:{0}) added to task (id:{1})  (user {2})'
                                 .format(task.id, _id, self.users[self.current_user].login))

    def change_user_task(self, _id, name=None, status=None, priority=None, finish=None, description=None,
                         deadline=None, period=None):
        """
            Function to change a task
            :param _id: task id of task to be change
            :param name: task new name
            :param status: task new status
            :param priority: task new priority
            :param finish: task new finish
            :param description: task new description
            :param deadline: task new deadline
            :param period: task new period
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot change task  because user not authorized.')
            raise Exception('You haven\'t authorized')
        if not deadline and period:
            if not self.users[self.current_user].get_task(_id).deadline:
                Logger.get_logger().error('Cannot change task because task is periodic and no deadline.')
                raise Exception('if task periodic, you must input deadline')

        if finish:
            try:
                parsed_finish = parse(finish, dayfirst=True)
                if parsed_finish < datetime.now(timezone.utc):
                    print(parsed_finish)
                    Logger.get_logger().error('Cannot change task because task is periodic and no deadline.')
                    raise Exception('task finish must be after now time')
            except:
                raise Exception('Wrong format of date')
        else:
            parsed_finish = finish

        if deadline:
            try:
                parsed_deadline = parse(deadline, dayfirst=True)
            except:
                raise Exception('Wrong format of date')
        else:
            parsed_deadline = deadline

        if int(priority) > 2 or int(priority) < 0:
            Logger.get_logger().error('Priority {0} doesn\'t exist'.format(priority))
            raise Exception('Wrong priority!')

        parsed_period = self.get_time_period(period)
        try:
            self.users[self.current_user].change_task(_id, name=name, status=status, priority=priority,
                                                      finish=parsed_finish, description=description,
                                                      deadline=parsed_deadline, period=parsed_period)
            self.storage.write_manager(self)
            Logger.get_logger().info('Task (id:{0}) changed'.format(_id))
        except Exception as e:
            Logger.get_logger().error('Task (id:{0}) dont change({1})'.format(_id, e))
            raise Exception(e)

    def delete_user_task(self, _id):
        """
            Delete a task with all its subtasks
            :param _id: task id of task to be removed
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot delete task  because user not authorized.')
            raise Exception('You haven\'t authorized')
        try:
            self.users[self.current_user].delete_task(_id)
            self.storage.write_manager(self)
            Logger.get_logger().info('Task (id:{0}) delete'.format(_id))
        except Exception as e:
            Logger.get_logger().error('Task (id:{0}) dont delete({1})'.format(_id, e))
            raise Exception(e)

    def complete_user_task(self, _id):
        """
            Function to complete a task.
            :param _id: id task to complete
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot complete task  because user not authorized.')
            raise Exception('You haven\'t authorized')
        self.users[self.current_user].complete_task(_id)
        self.storage.write_manager(self)
        Logger.get_logger().info('Task (id:{0}) is completed  (user {1})'
                                 .format(_id, self.users[self.current_user].login))

    def add_user_task_tag(self, _id, tag):
        """
            Function to add tags.
            :param _id: id task to add tag
            :param tag: new task tag
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot add task tag because user not authorized.')
            raise Exception('You haven\'t authorized')
        self.users[self.current_user].add_task_tag(tag, _id)
        self.storage.write_manager(self)
        Logger.get_logger().info('Add tag (#{0}) to Task (id:{1}) (user {2})'
                                 .format(tag, _id, self.users[self.current_user].login))

    def delete_user_task_tag(self, _id, tag):
        """
            Function to delete tags.
            :param _id: id task to delete tag
            :param tag: name removed tag
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot delete task tag because user not authorized.')
            raise Exception('You haven\'t authorized')
        self.users[self.current_user].delete_task_tag(tag, _id)
        self.storage.write_manager(self)
        Logger.get_logger().info('Delete tag (#{0}) from Task (id:{1}) (user {2})'
                                 .format(tag, _id, self.users[self.current_user].login))

    def print_user_tasks(self):
        """
            Function to give tasks
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        Logger.get_logger().info('Print all Tasks (user {0})'
                                 .format(self.users[self.current_user].login))
        tasks = self.users[self.current_user].print_tasks()
        return tasks

    def print_user_tasks_by_tag(self, tag):
        """
            Function to give tasks by tag
            :param tag: tag to print tasks
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        tasks = self.users[self.current_user].print_tasks_by_tag(tag)
        if tasks:
            Logger.get_logger().info('Print Tasks by tag (#{0}) (user {1})'
                                     .format(tag, self.users[self.current_user].login))
            return tasks
        else:
            Logger.get_logger().error('dont Print Tasks by tag (#{0}) (user {1})'
                                      .format(tag, self.users[self.current_user].login))
            raise Exception("tag {0} dont have tasks".format(tag))

    def print_user_archived_tasks(self):
        """
            Function to give archived tasks
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        tasks = self.users[self.current_user].print_archived_tasks()
        if tasks:
            Logger.get_logger().info('Print archived Tasks (user {0})'
                                     .format(self.users[self.current_user].login))
            return tasks
        else:
            Logger.get_logger().error('dont Print Tasks (user {0})'
                                      .format(self.users[self.current_user].login))
            raise Exception("dont have archived tasks")

    @staticmethod
    def get_time_period(period):
        """
        Make period to datetime format
        :param period:
        :return period in datetime format:
        """
        periods = {"h": relativedelta(hours=1),
                   "d": relativedelta(days=1),
                   "w": relativedelta(days=7),
                   "m": relativedelta(months=1),
                   "y": relativedelta(years=1)}
        return periods.get(period, None)

    def add_project(self, name):
        """
            Function to make a project
            :param name: task name
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        project = Project(name)
        project.add_user(self.current_user)
        self.current_project = str(project.id)
        self.projects[project.id] = project
        self.users[self.current_user].add_project(project)
        self.storage.write_manager(self)
        Logger.get_logger().info('Add new project (name:{0}) (id:{1})'.format(project.name, project.id))

    def print_projects(self):
        """
            Function to print projects
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        projects = self.users[self.current_user].get_projects()
        if projects:
            Logger.get_logger().info('Print all projects')
            return self.projects
        else:
            raise Exception("You dont have projects")

    def chose_current_project(self, _id):
        """
            Function to chose current project
            :param _id: project id
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        if _id in self.projects:
            self.current_project = _id
            self.storage.write_manager(self)
            Logger.get_logger().info('Chose current project: (name:{0}) (id:{1})'
                                     .format(self.projects[self.current_project].name, _id))
        else:
            Logger.get_logger().error('project with id:{0} not found!'.format(_id))
            raise Exception('Wrong id!')

    def add_project_user(self, login):
        """
            Function to add new user in project
            :param login: new user login
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('You haven\'t authorized')
            raise Exception('You haven\'t authorized')
        if not self.current_project:
            Logger.get_logger().error('You haven\'t authorize project')
            raise Exception('You haven\'t authorize project')
        user = User(login)
        user.tasks.tasks = self.projects[self.current_project].print_tasks()
        self.users[user.id] = user
        self.projects[self.current_project].add_user(user.id)
        self.storage.write_manager(self)
        Logger.get_logger().info('Project (name:{0}) (id:{1}) add user (login:{2}) (id:{3}) '
                                 .format(self.projects[self.current_project].name, self.current_project, user.login,
                                         user.id))
        Logger.get_logger().info('Add user {0} with id:{1}'.format(user.login, user.id))

    def add_project_available_user(self, _id):
        """
           Function to add available user in project
           :param _id: user id
           :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot add because user not authorized.')
            raise Exception('You haven\'t authorized')
        if _id in self.users:
            self.projects[self.current_project].add_user(_id)
            self.storage.write_manager(self)
            Logger.get_logger().info('Project (name:{0}) (id:{1}) add user (login:{2}) (id:{3}) '
                                     .format(self.projects[self.current_project].name,
                                             self.projects[self.current_project].id,
                                             self.users[_id].login, self.users[_id].id))
        else:
            Logger.get_logger().error('dont Add user with id:{0}'.format(_id))
            raise Exception('Wrong id! dont search user with id')

    def delete_project_user(self, _id):
        """
            Function to delete user from project
            :param _id: user id
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot delete because user not authorized.')
            raise Exception('You haven\'t authorized')
        if self.projects[self.current_project].create_user.id == self.current_user:
            if _id in self.projects[self.current_project].users:
                self.projects[self.current_project].delete_user(_id)
                self.storage.write_manager(self)
                Logger.get_logger().info('delete user with id:{0}'.format(_id))
            else:
                Logger.get_logger().error('dont delete user with id:{0}'.format(_id))
                raise Exception('Wrong id! dont delete user with id')
        else:
            raise Exception('You don have access')

    def get_project_users(self):
        """
            Function get current project users
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print users because user not authorized.')
            raise Exception('You haven\'t authorized')
        id_array = self.projects[self.current_project].users
        users = {}
        for _id in id_array:
            users[_id] = self.users[str(_id)]
        if users:
            Logger.get_logger().error('print project users')
            return users
        else:
            Logger.get_logger().error('dont have users')
            raise Exception('project dont have users')

    def add_project_task(self, name=None, priority=1, finish=None, description=None,
                         deadline=None, period=None):
        """
            Function to add a task to project
            :param name: task name
            :param priority: task priority
            :param finish: task finish
            :param description: task description
            :param deadline: task deadline
            :param period: task period
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot add task because user not authorized.')
            raise Exception('You haven\'t authorized')
        if self.current_user in self.projects[self.current_project].users:
            task = self.add_task(name=name, priority=priority, finish=finish, description=description,
                                 deadline=deadline, period=period)
        else:
            raise Exception(" You dont have access")
        self.projects[self.current_project].add_task(task, self.users)
        # self.users[self.current_user].add_task(task)
        self.storage.write_manager(self)
        Logger.get_logger().info('Task (id:{0}) added to priject {1}'.format(task.id, self.current_project))

    def add_project_available_task(self, _id):
        """
            Function to add a task to project
            :param _id: task id
            :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        self.projects[self.current_project].add_task(self.users[self.current_user].tasks.get_task(_id), self.users)
        self.storage.write_manager(self)
        Logger.get_logger().info('Task (id:{0}) added to project (id:{1})'
                                 .format(_id, self.projects[self.current_project].id))

    def delete_project_task(self, _id):
        """
           Function to delete task from project
           :param _id: task id
           :return: None
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        self.projects[self.current_project].delete_task(_id)
        self.storage.write_manager(self)
        Logger.get_logger().info('Task (id:{0}) delete from project (id:{1})'
                                 .format(_id, self.projects[self.current_project].id))

    def print_project_tasks(self):
        """
            Function to add a task to project
            :return: tasks
        """
        if not self.current_user:
            Logger.get_logger().error('Cannot print task because user not authorized.')
            raise Exception('You haven\'t authorized')
        return self.projects[self.current_project].print_tasks()
