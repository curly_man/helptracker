import jsonpickle
import os


class Storage:
    def __init__(self, folder, mngr):
        self.folder = folder
        self.mngr = mngr

    def write_manager(self, manager):
        try:
            temp = jsonpickle.dumps(manager)
            with open(os.path.join(os.path.dirname(os.getcwd()), self.folder, self.mngr), 'w+') as file:
                file.write(temp)
        except:
            os.mkdir(os.path.dirname(os.getcwd()) + '\data')
            self.write_manager(manager)

    def read_manager(self):

        try:
            if os.path.exists(os.path.join(os.path.dirname(os.getcwd()), self.folder, self.mngr)):
                with open(os.path.join(os.path.dirname(os.getcwd()), self.folder, self.mngr), 'r+') as file:
                    manager = jsonpickle.loads(file.read())
                    return manager
        except:
            raise Exception('manager.JSON don\'t have information')
