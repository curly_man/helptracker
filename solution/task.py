from datetime import datetime, timezone
from solution.status import STATUS
from solution.priority import PRIORITIES



class Task:
    """
            Class that just stores single Task attributes
            name - a name of task
            subtask - child tasks connected to a task.
            parent - id of parent task
            tags - user defined key_words
            start - when the task starts
            finish - when the task ends
            status - one of fixed statuses
            priorities- one of fixed in priorities
            id - unique id of a task
            period - the interval for which the task is postponed
            deadline - used for periodic task
            description - some description of the task
            archived - used for completed task task
    """

    def __init__(self, name=None, status=0, priority=1, finish=None,
                 description=None, period=None, deadline=None, subtasks=None):
        self.name = name
        self.status = STATUS[int(status)]
        self.priority = PRIORITIES[int(priority)]
        self.start = datetime.now()
        self.period = period
        self.deadline = deadline
        self.finish = finish
        self.description = description
        self.parent = False
        self.id = hash(self)
        self.tags = []
        if not subtasks:
            from solution.alltasks import AllTasks
            self.subtask = AllTasks()
        else:
            self.subtask = subtasks
        self.archived = False


def start_task(task):
    """
        For periodic task.
        :param _id: id task to start
        :return: None
    """
    if datetime.now(timezone.utc) < task.finish:
        if task.deadline > task.finish:
            task.status = STATUS[2]
        else:
            new_task = Task(name=task.name, description=task.description, finish=task.finish,
                            deadline=task.deadline, period=task.period, priority=1,
                            subtasks=task.subtask)
            new_task.deadline = task.deadline + task.period
            while task.deadline < datetime.now(timezone.utc):
                new_task.deadline = task.deadline + task.period
                new_task.status = STATUS[0]
            return new_task
